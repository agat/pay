import test from 'ava';

import {
  getCashInCommission,
  getCashOutJuridicalCommission,
  getCashOutNaturalCommission,
} from '../src/utils/commission';

import operations from '../input';


test('Calculate cashIn commission', (t) => {
  const config = {
    percents: 0.03,
    max: {
      amount: 5,
      currency: 'EUR',
    },
  };

  t.true(getCashInCommission(operations[0], config) === '0.06');
});

test('Calculate cashOut commission for Juridical', (t) => {
  const config = {
    percents: 0.3,
    min: {
      amount: 0.5,
      currency: 'EUR',
    },
  };

  t.true(getCashOutJuridicalCommission(operations[1], config) === '0.90');
});

test('Calculate cashOut commission for Natural', (t) => {
  const config = {
    percents: 0.3,
    week_limit: {
      amount: 1000,
      currency: 'EUR',
    },
  };

  t.true(getCashOutNaturalCommission(operations, 3, config) === '3.00');
  t.true(getCashOutNaturalCommission(operations, 5, config) === '0.30');
});
