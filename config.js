// @flow

export default {
  api: {
    config: {
      cashIn: 'http://private-38e18c-uzduotis.apiary-mock.com/config/cash-in',
      cashOut: {
        natural: 'http://private-38e18c-uzduotis.apiary-mock.com/config/cash-out/natural',
        juridical: 'http://private-38e18c-uzduotis.apiary-mock.com/config/cash-out/juridical',
      },
    },
  },
};
