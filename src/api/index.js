// @flow
import fetch from 'node-fetch';

import config from '../../config';


const apiConfig = config.api.config;


const getApiCall = <T>(url: string):Promise<T> => fetch(url).then(res => res.json());


export type CashInT = {
  percents: number,
  max: {
    amount: number,
    currency: string
  }
};

export const getCashInCfg = () => getApiCall<CashInT>(apiConfig.cashIn);


export type CashOutNaturalT = {
  percents: number,
  week_limit: {
    amount: number,
    currency: string
  }
};

export const getCashOutForNaturalCfg = () => getApiCall<CashOutNaturalT>(apiConfig.cashOut.natural);


export type CashOutJuridicalT = {
  percents: number,
  min: {
    amount: number,
    currency: string
  }
};

export const getCashOutForJuridicalCfg = () => getApiCall<CashOutJuridicalT>(apiConfig.cashOut.juridical);


export type ConfigT = {
  in: CashInT,
  out: {
    natural: CashOutNaturalT,
    juridical: CashOutJuridicalT
  }
};

export const getConfig = (): Promise<ConfigT> => Promise.all([
  getCashInCfg(),
  getCashOutForNaturalCfg(),
  getCashOutForJuridicalCfg(),
]).then(([
  cashInConfig,
  cashOutForNaturalConfig,
  cashOutForJuridicalConfig,
]: [
  CashInT,
  CashOutNaturalT,
  CashOutJuridicalT
]) => ({
  in: cashInConfig,
  out: {
    natural: cashOutForNaturalConfig,
    juridical: cashOutForJuridicalConfig,
  },
}));

export default {
  getCashInCfg,
  getCashOutForNaturalCfg,
  getCashOutForJuridicalCfg,
};
