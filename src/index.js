// @flow

import {
  getConfig,
} from './api';

import {
  getCommission,
} from './utils/commission';

import {
  type OperationT,
} from './types';


const [pathToData] = process.argv.slice(2);

if (pathToData) {
  const operations = require(pathToData);

  getConfig().then((config) => {
    operations.forEach((op: OperationT, index) => {
      console.log(getCommission(operations, index, config));
    });
  });
}
