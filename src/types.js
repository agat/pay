// @flow

import {
  OPERATION_TYPE,
} from './constants';

export type UserType = 'natural' | 'juridical';

export type OperationTypeT = $Values<typeof OPERATION_TYPE>;

export type UserT = {
  user_id: number,
  user_type: UserType
};

export type OperationT = UserT & {
  date: string,
  type: OperationTypeT,
  operation: {
    amount: number,
    currentcy: string
  }
};

export type UserOperationsT = UserT & {
  operations: OperationT[]
};
