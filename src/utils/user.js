/* eslint-disable max-len */
/* eslint-disable camelcase */
// @flow

import {
  type OperationT,
  type OperationTypeT,
} from '../types';

export const filterUserOperations = (user_id: number, type: OperationTypeT, operations: OperationT[]): OperationT[] => operations.filter(op => op.user_id === user_id && op.type === type);

export default {
  filterUserOperations,
};
