/* eslint-disable max-len */
// @flow
import currency from 'currency.js';

import {
  type OperationT,
} from '../types';

import {
  type CashInT,
  type CashOutJuridicalT,
  type CashOutNaturalT,
  type ConfigT,
} from '../api';

import {
  filterUserOperations,
} from './user';
import {
  parseDateString,
  getUTCDate,
  getMondayOfWeek,
} from './date';

import {
  OPERATION_TYPE,
} from '../constants';


const calculatePercentage = (value: number, percentage: number) => value * (percentage / 100);

const roundAndFormatCurrency = (value: number): string => currency(value, { precision: 2 }).format(false);

const filterLastWeekOperations = (operations: OperationT[]): OperationT[] => {
  const lastOperation = operations[operations.length - 1];
  const weekStart = getMondayOfWeek(getUTCDate(...parseDateString(lastOperation.date)));

  return operations.filter(({ date }) => getUTCDate(...parseDateString(date)).getTime() >= weekStart.getTime());
};


export const getCashInCommission = (op: OperationT, config: CashInT): string => {
  let commission = calculatePercentage(op.operation.amount, config.percents);

  if (commission > config.max.amount) {
    commission = config.max.amount;
  }

  return roundAndFormatCurrency(commission);
};

export const getCashOutJuridicalCommission = (op: OperationT, config: CashOutJuridicalT): string => {
  let commission = calculatePercentage(op.operation.amount, config.percents);

  if (commission < config.min.amount) {
    commission = config.min.amount;
  }

  return roundAndFormatCurrency(commission);
};


export const getCashOutNaturalCommission = (operations: OperationT[], index: number, config: CashOutNaturalT): string => {
  const userOperation = operations[index];
  const pastOperations = operations.slice(0, index + 1);
  const userOperations = filterUserOperations(userOperation.user_id, OPERATION_TYPE.CASHE_OUT, pastOperations);
  const lastWeekOperations = filterLastWeekOperations(userOperations);
  const operationsSumReducer = (sum, op) => sum + op.operation.amount;
  const getCommission = (sum: number) => roundAndFormatCurrency(calculatePercentage(sum, config.percents));

  // If not operations on current week, commission 0
  if (lastWeekOperations.length === 0) {
    return roundAndFormatCurrency(0);
  }

  const [lastOperation, ...prevOperations] = lastWeekOperations.reverse();
  const prevOperationsSum = prevOperations.reduce(operationsSumReducer, 0);
  const allOperationsSum = lastWeekOperations.reduce(operationsSumReducer, 0);

  // If previous operations sum reach week limit, calculate last operation commission
  if (prevOperationsSum > config.week_limit.amount) {
    return getCommission(lastOperation.operation.amount);
  }

  // If all operations sum reach week limit, calculate sum - week limit commisssion
  if (allOperationsSum > config.week_limit.amount) {
    return getCommission(allOperationsSum - config.week_limit.amount);
  }

  return roundAndFormatCurrency(0);
};


// Calculate all operations commission, depending of the operation type and user_type
export const getCommission = (operations: OperationT[], index: number, config: ConfigT): string => {
  const operation = operations[index];

  switch (operation.type) {
    case 'cash_in':
      return getCashInCommission(operation, config.in);

    case 'cash_out':
      switch (operation.user_type) {
        case 'juridical':
          return getCashOutJuridicalCommission(operation, config.out.juridical);
        case 'natural':
          return getCashOutNaturalCommission(operations, index, config.out.natural);

        default:
          return roundAndFormatCurrency(0);
      }
    default:
      return roundAndFormatCurrency(0);
  }
};

export default {
  getCommission,
};
