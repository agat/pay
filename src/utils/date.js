/* eslint-disable max-len */
// @flow

export const parseDateString = (date: string) => {
  const [sYear, sMonth, sDay] = date.split('-');
  const year = parseInt(sYear, 10);
  const month = parseInt(sMonth, 10);
  const day = parseInt(sDay, 10);

  return [year, month, day];
};

export const getUTCDate = (year: number, month: number, day: number) => new Date(Date.UTC(year, month - 1, day));

export const getMondayOfWeek = (date: Date) => {
  const day = date.getDay();
  const diff = date.getDate() - day + (day === 0 ? -6 : 1);

  return new Date(date.setDate(diff));
};
