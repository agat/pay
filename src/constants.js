// @flow

export const OPERATION_TYPE = Object.freeze({
  CASHE_IN: 'cash_in',
  CASHE_OUT: 'cash_out'
});
