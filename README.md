Require `node` and 'npm' or `yarn` installed in system.

In project directory run `yarn` on `npm i`.

`yarn start` - to run with default data file, or `babel-node ./src/index.js ../input/index.json`.

`yarn test` to run tests.
